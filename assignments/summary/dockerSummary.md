# **Docker**

## What is Docker?
Docker is a program which runs various applications on the containers.

## Why Docker?
For developers, there arises a situation where they need to work on different projects. And for each different project they might need a specific operating system, or a specific version of operating system.
This becomes a tedious job to do. This problem can be solved using virtual machines, but this also requires exact enviornment to run an application. 
Here comes the concept of Docker. The developer can work on the application and save it as docker image and create a docker container. 
This container has everything which is required to run an application alongwith the enviornment.
This docker is then stored to repository and then can be used by other developers.   So docker is used by many developers.

## Docker Terms
**1. Docker Image**  
The Container is a block where all the specifications and enviornment required to run an application is stored.
The image can be employed to any Docker enviornment as container.
* Code.
* Runtime.
* Enviornment.
* Libraries.
* Configuration Files. 

**2. Docker Container**  
The container is a block where image is saved.   
Many container can be created using single image.  

**3. DockerHub**  
It's like Github of Docker.  
Dockerhub stores images and containers. 

## Difference between Docker and Virtual Machine:
**Docker:**  
Docker runs on the system using image where all the files are availble to run the processes.  
It share the space with the Host OS.  
**Virtual Machine:**  
It installs the different operating system on host OS and also requires additional user space.   
So in vm there are also various files which are unwanted to run the process. While in docker it has what it needs and no extra things.

<img src="https://cdn.rancher.com/wp-content/uploads/2017/02/16175231/VMs-and-Containers.jpg" height="300" width="700">


## Docker Components:  
1. Docker Engine:  
The place where containers and runs the programs.  
2. Docker Client:  
It is the end user which provides the demands of the docker file as commands.  
3. Docker daemon:  
This place checks the client request and communicate with docker components such as image, container, to perform the process.  
4. Docker Registry:  
A place docker images are stored. DockerHub is such a public registry.  

<img src="https://i0.wp.com/cdn-images-1.medium.com/max/1600/1*bIQOZL_pZujjrfaaYlQ_gQ.png?w=810&ssl=1" height="400" width="700">

[More About Docker!!](https://www.docker.com/resources/what-container)

## Basic Docker Commands
1. Pull:
To pull images from docker repository.
> $ Docker pull (image name)

2. Run:
To create container from image.
> $ docker run -it-d (image name)

3. Ps:
To list the running containers.
> $ docker ps

4.Stop: 
To stop running container
> $ docker stop (container id)

5.Push:
To push the images to docker repossitory
> $ docker push (username/imagename) 

[Docker Commands Reference](https://www.edureka.co/blog/docker-commands/)
