# **Git**

## What is Git and Why Git?
* Git is distributed version-control system for tracking changes in code of a project.
* It allows acces to work on the files from anywhere and the upload it to remote repository.
* A group of people can work on the same project and contribute.
* Git also provides to work with branches.

### Git workflow

1.**Working Area**:     
It is folder/directory where We create and edit files in this area.  
2.**Staging Area**:    
It is the place where we save files temporarily.  
3.**Local Repository**:  
Here we commit the files after working on them. Local repository is the offline version of remoter repository.  
4.**Remote Repository**:  
The place where the things are stored over the cloud.  
* _This is the workflow of git_:  
<img src="https://res.cloudinary.com/practicaldev/image/fetch/s--M_fHUEqA--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://thepracticaldev.s3.amazonaws.com/i/128hsgntnsu9bww0y8sz.png" height="500" width="700" align="centre">  


### Branch
Branch means we create a special section on the main code and work on it.  
We can do changes to it without affecting the main .
After the work is done satisfactorily we can merge our work into main branch.  
* _Branching of git_:   
<img src="https://www.nobledesktop.com/image/classExamples/git/branches.png
" height="500" width="700" align="centre">



## Basic Git commands
**These are the commands for terminal**
1. Configuration.  
This will setup the get in machine
> $ git config  --global user.name 'name-of -the-user'
> $ git config --global user.email 'email-id'

2. Initialize.  
This will create a .git directory in the local machine.
> $ git init

3. Clone.  
Cloning the remote repo to local machine.  
> $ git clone (link-to-repo)

4. Status.  
Check the status of files in staging area.
> $ git status <this will show either files are staged or committed>

5. Add.  
To add the files to the staging area.
> $ git add . <this dot will add all the files to staging area.>

6. Commit.  
To save the files from staging area to local rpository.
> $ git commit -m (description-of-commit)

7. Pull.  
This will pull the files from remote repo to local machine.
> $ git pull

8. Branch  
Checkout will change/show the current working branch.  
Checkout -b will create a new branch.  
> $ git checkout <this will show the current branch>
> $ git checkout -b (branch-name) <create a new branch with given name>

9. Push.  
This will push the local machines file to remote repository.
> $ git push origin (branch-name)

We will be using Gitlab for remote repository.
